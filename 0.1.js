const app = new Vue({
    el: '#app',
    data:{
        titulo: 'hola mundo con vue',
        frutas: [
            { nombre: 'aguacate', cantidad:10 },
            { nombre: 'pera', cantidad: 22 },
            { nombre: 'maracuya', cantidad: 0 }
        ],
        nuevaFruta: "",
        total:0,
        fondo: "bg-warning",
        color: false

        
        
    },
    methods:{
        agregarFruta (){
           this.frutas.push({
               nombre: this.nuevaFruta,
               cantidad: 0
                
           });
           this.nuevaFruta = ""
          
        }
    },
    computed:{
        sumarFrutas(){
            this.total = 0;
            for (fruta of this.fruta){
                this.total = this.total + fruta.cantidad;

            }
        }
    }
})